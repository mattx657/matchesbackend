const { Pool } = require("pg");
const dotenv = require("dotenv");
dotenv.config();

// const pool = new Pool({
//   user: "postgres",
//   password: "superuser",
//   host: "localhost",
//   port: 5432, // default Postgres port
//   database: "euro24",
// });

const pool = new Pool({
  user: process.env.DB_USER || "postgres",
  password: process.env.DB_PASSWORD || "superuser",
  host: process.env.DB_HOST || "localhost",
  port: 5432,
  database: process.env.DB_HOST ? "kickoff" : "euro24",
  ssl: process.env.DB_HOST ? true : false,
});

module.exports = {
  query: (text, params) => pool.query(text, params),
};
