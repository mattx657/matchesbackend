var express = require("express");
var router = express.Router();
const db = require("../db");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const verifyToken = require("./middleware/authMiddleware");

/* GET users listing. */
router.get("/", verifyToken, async function (req, res, next) {
  const result = await db.query(
    `SELECT  id,email,firstName,lastName,SUM (
    CASE WHEN score.point >0 THEN score.point ELSE 0 END
  ) as score FROM player as p
  left join LATERAL (  SELECT
				   1 as point,playerid
				   
				   FROM game_player as gp 

JOIN  game as g on g.id =gp.gameid and g.won_country_id = gp.betcountryid
	WHERE
playerid=p.id) as score ON score.playerid=p.id
 WHERE id=$1
GROUP BY p.id,score.point,p.FirstName,p.LastName

`,
    [req.userId]
  );
  res.json(result.rows[0]);
});
router.post("/create", async function (req, res, next) {
  //console.log(req);
  const body = req.body;
  try {
    const saltRounds = 10;
    await bcrypt.hash(body.password, saltRounds, async function (err, hash) {
      const result = await db.query(
        `INSERT INTO player (email,password,firstName,lastName)VALUES('${body.email}','${hash}','${body.firstName}','${body.lastName}')`
      );
    });

    // res.json(result.rows);
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
    return;
  }
  res.send("User saved ");
});
// module.exports = router;
/* login */

router.post("/login", async function (req, res, next) {
  const { email, password } = req.body;
  try {
    const result = await db.query(
      `SELECT id,password,firstName,lastName FROM player WHERE email = $1`,
      [email]
    );
    const user = result.rows[0];

    if (!user) {
      return res.status(401).send("User not found");
    }
    bcrypt.compare(password, user.password, function (err, result) {
      console.log(result);
      if (result) {
        const token = jwt.sign({ userId: user.id }, "your-secret-key", {
          expiresIn: "355d",
        });
        res.json({
          token: token,
          email: user.email,
          firstName: user.firstName,
          lastName: user.lastName,
        });
      } else {
        res.status(401).send("Invalid login details");
      }
    });
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
  }
});

router.post("/result", async function (req, res, next) {
  const { userId, gameId, betCountryId } = req.body;
  try {
    const result = await db.query(
      `INSERT INTO game_player (gameId, playerId, betCountryId) VALUES ($1, $2, $3) RETURNING id`,
      [gameId, userId, betCountryId]
    );
    const newBetId = result.rows[0].id;
    res.json({ message: "Result added", betId: newBetId });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

module.exports = router;
