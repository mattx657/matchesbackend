var express = require("express");
var router = express.Router();
const db = require("../db");
const verifyToken = require("./middleware/authMiddleware");

router.post("/", verifyToken, async function (req, res, next) {
  const {
    gameId,
    betCountryId,
    betExtraTime,
    betPenaltyResult,
    bet_country_phase_id,
  } = req.body;

  try {
    const currentDate = new Date().setHours(new Date().getHours() + 2);
    const game = (
      await db.query(
        `SELECT id, game_date
         FROM public.game
         WHERE id = $1`,
        [gameId]
      )
    ).rows[0];

    if (
      currentDate >
      new Date(game.game_date).setHours(game.game_date.getHours() - 1)
    ) {
      console.log("Can't save");
      res.status(405).send();
      return;
    }

    const existingBet = await db.query(
      `SELECT id
       FROM public.game_player
       WHERE playerid = $1 AND gameid = $2`,
      [req.userId, gameId]
    );

    if (existingBet.rows.length > 0) {
      await db.query(
        `UPDATE game_player
         SET betcountryid = $1, betextratime = $2, betpenaltyresult = $3, bet_country_phase_id = $4
         WHERE playerid = $5 AND gameid = $6`,
        [
          betCountryId,
          betExtraTime,
          betPenaltyResult,
          bet_country_phase_id,
          req.userId,
          gameId,
        ]
      );
      return res.json({
        message: "Bet updated",
        betId: existingBet.rows[0].id,
      });
    } else {
      const newBet = await db.query(
        `INSERT INTO game_player (gameid, playerid, betcountryid, betextratime, betpenaltyresult, bet_country_phase_id)
         VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`,
        [
          gameId,
          req.userId,
          betCountryId,
          betExtraTime,
          betPenaltyResult,
          bet_country_phase_id,
        ]
      );
      return res.json({ message: "Bet added", betId: newBet.rows[0].id });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

router.get("/gamePlayer", verifyToken, async function (req, res, next) {
  try {
    const gamePlayer = await db.query(
      `SELECT id, gameid, betcountryid, playerid, betextratime, betpenaltyresult, bet_country_phase_id as phaseGameId
       FROM game_player
       WHERE playerid = $1`,
      [req.userId]
    );
    res.json(gamePlayer.rows);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

router.get("/currentTime", function (req, res, next) {
  const currentDate = new Date().setHours(new Date().getHours() + 2);
  res.status(200).send(currentDate);
});

router.get("/getAllCountry", async function (req, res, next) {
  try {
    const countries = await db.query(
      `SELECT id, title, countryimg FROM country`
    );
    res.json(countries.rows);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

router.get("/game", async function (req, res, next) {
  try {
    const games = (
      await db.query(
        `SELECT id, country_1, country_2, game_date, won_country_id, won_phase_game_id, country_score_1, country_score_2
         FROM game
         ORDER BY game_date`
      )
    ).rows;

    const gamePredictions = (
      await db.query(
        `SELECT gameid, SUM(CASE WHEN betcountryid >= 0 THEN 1 ELSE 0 END) AS sum, betcountryid, betextratime, betpenaltyresult, bet_country_phase_id
         FROM game_player
         GROUP BY gameid, betcountryid, betextratime, betpenaltyresult, bet_country_phase_id
         ORDER BY gameid`
      )
    ).rows;

    games.forEach((game) => {
      const gamePred = gamePredictions.filter(
        (prediction) => prediction.gameid === game.id
      );

      gamePred.forEach((prediction) => {
        if (prediction.betcountryid === game.country_1) {
          game.predCountry_1 = prediction.sum;
        } else if (prediction.betcountryid === game.country_2) {
          game.predCountry_2 = prediction.sum;
        } else if (prediction.betextratime === game.country_1) {
          game.predExtraTime = prediction.sum;
        } else if (prediction.betpenaltyresult === game.country_2) {
          game.predPenaltyResult = prediction.sum;
        } else if (prediction.betpenaltyresult === game.country_1) {
          game.predPenaltyResult = prediction.sum;
        } else if (prediction.betextratime === game.country_2) {
          game.predExtraTime = prediction.sum;
        }
      });
    });
    res.json(games);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

router.get("/:countryId", verifyToken, async function (req, res, next) {
  const countryId = req.params.countryId;

  try {
    const country = await db.query(`SELECT title FROM country WHERE id = $1`, [
      countryId,
    ]);

    if (country.rows.length === 0) {
      res.status(404).json({ error: "Country not found" });
      return;
    }

    const countryName = country.rows[0].title;
    res.json({ countryName: countryName });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

module.exports = router;
