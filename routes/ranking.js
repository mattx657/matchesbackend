var express = require("express");
var router = express.Router();
const db = require("../db");
const verifyToken = require("./middleware/authMiddleware");

router.get("/", async function (req, res, next) {
  //   const players = await db.query(
  //     `SELECT
  // p.FirstName,
  // p.LastName,
  // p.id,
  //  CAST(SUM (
  //     CASE WHEN score.point >0 THEN score.point ELSE 0 END
  //  ) as INTEGER) as score
  // FROM player as p
  // left join LATERAL (  SELECT
  // 				   1 as point,playerid

  // 				   FROM game_player as gp

  // JOIN  game as g on g.id =gp.gameid and g.won_country_id = gp.betcountryid
  // 	WHERE
  // playerid=p.id) as score ON score.playerid=p.id

  // GROUP BY p.id,score.point,p.FirstName,p.LastName
  // ORDER BY score desc

  //  `
  //   );
  const players = (await db.query(`SELECT id,firstname,lastname FROM player`))
    .rows;
  players.forEach((player) => {
    player.score = 0;
  });
  const gameplayers = (
    await db.query(`SELECT id,betcountryid,bet_country_phase_id,gameid,playerid FROM game_player
`)
  ).rows;
  const games = await db.query(
    `SELECT id,country_1,country_2,won_country_id,won_phase_game_id FROM game where won_country_id is not null`
  );
  console.log(games);
  for (let i = 0; i < games.rows.length; i++) {
    const game = games.rows[i];
    const gameplayerCorrects = gameplayers.filter(
      (x) => x.gameid === game.id && x.betcountryid === game.won_country_id
    );

    for (let index = 0; index < gameplayerCorrects.length; index++) {
      const gameplayer = gameplayerCorrects[index];

      const player = players.find((x) => x.id === gameplayer.playerid);

      if (player) {
        if (
          gameplayer.bet_country_phase_id < 1 ||
          gameplayer.bet_country_phase_id == null
        ) {
          player.score++;
        } else if (
          gameplayer.bet_country_phase_id > 0 &&
          gameplayer.bet_country_phase_id === game.won_phase_game_id
        ) {
          player.score += 5;
        }
      }
    }
  }
  res.json(players);
});

module.exports = router;
