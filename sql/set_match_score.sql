--SELECT id, country_1, country_2, game_date, won_country_id, country_score_1, country_score_2
	--FROM public.game;
	SELECT g.id, c.title country1,country_1, c2.title country_2,country_2, game_date, won_country_id, country_score_1, country_score_2
	FROM public.game g
	JOIN country c on c.id =g.country_1
	JOIN country c2 on c2.id =g.country_2
	where game_date::date =CURRENT_DATE


UPDATE game
	SET won_country_id=10,country_score_1=2,country_score_2=0
WHERE id=5 
	